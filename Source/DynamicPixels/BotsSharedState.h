#pragma once

#include "CoreMinimal.h"
#include "BotBehavior.h"

class DYNAMICPIXELS_API BotsSharedState
{
private:
	BotsSharedState();

	BotBehaviorState BehaviorState;

	void logBehaviorState();

public:
	static BotsSharedState &GetInstance();

	void Reset();

	BotBehaviorState GetBehaviorState();
	void SetNextBehaviorState();
};
