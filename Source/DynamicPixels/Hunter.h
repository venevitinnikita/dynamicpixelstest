#pragma once

#include "CoreMinimal.h"
#include "Bait.h"

class Hunter
{
  private:
	FVector Location;
	FRotator Rotation;

	ABait *NearBait;
	ABait *OwnedBait;

  protected:
	void UpdateSelfPosition(FVector Location, FRotator Rotation);

  public:
	Hunter();

	bool IsHoldingBait();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector HoldingObjectOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float ThrowDistance;

	virtual void AllowGrabBait(ABait *Bait);
	void DisallowGrabBait();
	virtual void UpdateBaitLocation();

	void Grab();
	void Throw();
};
