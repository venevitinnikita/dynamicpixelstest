#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class BotBehaviorState : uint8
{
	HuntingForBait UMETA(DisplayName = "HuntingForBait"),
	HuntingForPlayer UMETA(DisplayName = "HuntingForPlayer"),
	Surrounding UMETA(DisplayName = "Surrounding"),
	Waiting UMETA(DisplayName = "Waiting")
};