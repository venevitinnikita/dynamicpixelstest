#include "Bait.h"
#include "PlayerCharacter.h"
#include "Bot.h"

ABait::ABait()
{
	USphereComponent *OverlapSphere;
	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("OverlapSphere"));
	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &ABait::OnStartOverlap);
	OverlapSphere->OnComponentEndOverlap.AddDynamic(this, &ABait::OnEndOverlap);
	OverlapSphere->InitSphereRadius(150.0f);
}

void ABait::OnStartOverlap(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult)
{
	if (!IsOwned())
	{
		APlayerCharacter *Player = Cast<APlayerCharacter>(OtherActor);
		ABot *Bot = Cast<ABot>(OtherActor);

		if (Player)
		{
			Player->AllowGrabBait(this);
		}
		else if (Bot)
		{
			Bot->AllowGrabBait(this);
		}
	}
}

void ABait::OnEndOverlap(
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex)
{
	APlayerCharacter *Player = Cast<APlayerCharacter>(OtherActor);
	ABot *Bot = Cast<ABot>(OtherActor);

	if (Player)
	{
		Player->DisallowGrabBait();
	}
	else if (Bot)
	{
		Bot->DisallowGrabBait();
	}
}

void ABait::SetOwned(bool bOwned)
{
	this->bOwned = bOwned;
}

bool ABait::IsOwned()
{
	return bOwned;
}