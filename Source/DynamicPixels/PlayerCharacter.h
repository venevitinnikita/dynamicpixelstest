#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Bait.h"
#include "Hunter.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class DYNAMICPIXELS_API APlayerCharacter : public ACharacter, public Hunter
{
	GENERATED_BODY()

private:
	bool bBlocked;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void StartJump();
	void StopJump();

protected:
	virtual void BeginPlay() override;

public:
	APlayerCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	void SetBlocked(bool bBlocked);
};
