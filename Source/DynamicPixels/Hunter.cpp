#include "Hunter.h"

Hunter::Hunter()
{
    HoldingObjectOffset = FVector(80.0f, 0.0f, 40.0f);
    ThrowDistance = 500.0f;
}

void Hunter::UpdateSelfPosition(FVector Location, FRotator Rotation)
{
    this->Location = Location;
    this->Rotation = Rotation;
}

void Hunter::AllowGrabBait(ABait *Bait)
{
    this->NearBait = Bait;
}

void Hunter::DisallowGrabBait()
{
    this->NearBait = nullptr;
}

void Hunter::Grab()
{
    if (NearBait && !NearBait->IsOwned())
    {
        OwnedBait = NearBait;
        OwnedBait->SetOwned(true);
    }
}

void Hunter::Throw()
{
    if (OwnedBait)
    {
        FVector PlaceLocation = Location +
                                FTransform(Rotation).TransformVector(FVector(ThrowDistance, 0.0f, 0.0f));

        PlaceLocation.Z = 40.0f;

        FHitResult HitResult;
        OwnedBait->K2_SetActorLocation(PlaceLocation, false, HitResult, true);

        OwnedBait->SetOwned(false);
        OwnedBait = nullptr;
    }
}

void Hunter::UpdateBaitLocation()
{
    if (OwnedBait)
    {
        FVector ObjectLocation = Location + FTransform(Rotation).TransformVector(HoldingObjectOffset);

        FHitResult HitResult;
        OwnedBait->K2_SetActorLocation(ObjectLocation, false, HitResult, true);
    }
}

bool Hunter::IsHoldingBait()
{
    return OwnedBait != nullptr;
}