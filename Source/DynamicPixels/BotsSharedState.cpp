#include "BotsSharedState.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

BotsSharedState::BotsSharedState()
{
    Reset();
}

BotsSharedState &BotsSharedState::GetInstance()
{
    static BotsSharedState Instance;
    return Instance;
}

void BotsSharedState::Reset()
{
    if (BehaviorState != BotBehaviorState::HuntingForBait)
    {
        BehaviorState = BotBehaviorState::HuntingForBait;
        logBehaviorState();
    }
}

BotBehaviorState BotsSharedState::GetBehaviorState()
{
    return BehaviorState;
}

void BotsSharedState::SetNextBehaviorState()
{
    switch (BehaviorState)
    {
    case BotBehaviorState::HuntingForBait:
        BehaviorState = BotBehaviorState::HuntingForPlayer;
        logBehaviorState();
        break;
    case BotBehaviorState::HuntingForPlayer:
        BehaviorState = BotBehaviorState::Surrounding;
        logBehaviorState();
        break;
    case BotBehaviorState::Surrounding:
        BehaviorState = BotBehaviorState::Waiting;
        logBehaviorState();
        break;
    case BotBehaviorState::Waiting:
        BehaviorState = BotBehaviorState::HuntingForBait;
        logBehaviorState();
        break;
    default:
        UE_LOG(LogTemp, Display, TEXT("Unimplemented bot behavior"));
        return;
    }
}

void BotsSharedState::logBehaviorState()
{
    switch (BehaviorState)
    {
    case BotBehaviorState::HuntingForBait:
        UE_LOG(LogTemp, Display, TEXT("Hunting for bait"));
        break;
    case BotBehaviorState::HuntingForPlayer:
        UE_LOG(LogTemp, Display, TEXT("Hunting for player"));
        break;
    case BotBehaviorState::Surrounding:
        UE_LOG(LogTemp, Display, TEXT("Surrounding"));
        break;
    case BotBehaviorState::Waiting:
        UE_LOG(LogTemp, Display, TEXT("Waiting"));
        break;
    }
}