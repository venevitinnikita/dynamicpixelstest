#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Hunter.h"
#include "Bait.h"
#include "BotBehavior.h"
#include "Bot.generated.h"

UCLASS()
class DYNAMICPIXELS_API ABot : public APawn, public Hunter
{
	GENERATED_BODY()

private:
	BotBehaviorState BehaviorState;

public:
	ABot();

	void SetBehavior(BotBehaviorState BehaviorState);

	BotBehaviorState GetBehavior();

	virtual void AllowGrabBait(ABait *Bait) override;

	virtual void UpdateBaitLocation() override;
};
