#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DynamicPixelsGameModeBase.generated.h"

UCLASS()
class DYNAMICPIXELS_API ADynamicPixelsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
