#include "BotController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

template <typename T>
T *FindFirstActorOnScene(UWorld *World);

int ABotController::Count = 0;

ABotController::ABotController()
{
	PrimaryActorTick.bCanEverTick = true;

	WaitingDuration = 5.0f;
	TimeoutAfterCatch = 5.0f;
	PlayerCatchDistance = 300.0f;
	PlayerSurroundRadius = 500.0f;

	Index = Count++;
}

void ABotController::BeginPlay()
{
	Super::BeginPlay();

	Bait = FindFirstActorOnScene<ABait>(GetWorld());
	Player = FindFirstActorOnScene<APlayerCharacter>(GetWorld());

	BotsSharedState::GetInstance().Reset();
	Cast<ABot>(GetPawn())->SetBehavior(BotsSharedState::GetInstance().GetBehaviorState());
}

void ABotController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult &Result)
{
	ABot *Bot = Cast<ABot>(GetPawn());
	switch (BotsSharedState::GetInstance().GetBehaviorState())
	{
	case BotBehaviorState::HuntingForBait:
		if (Bot->IsHoldingBait())
		{
			BotsSharedState::GetInstance().SetNextBehaviorState();
		}
		break;
	case BotBehaviorState::HuntingForPlayer:
		if (Bot->IsHoldingBait())
		{
			FVector DistanceToPlayer = Bot->GetActorLocation() - Player->GetActorLocation();
			if (DistanceToPlayer.Size() < PlayerCatchDistance)
			{
				Bot->Throw();
				BotsSharedState::GetInstance().SetNextBehaviorState();
			}
		}
		break;
	}
}

void ABotController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ABot *Bot = Cast<ABot>(GetPawn());

	BotBehaviorState OldBehavior = Bot->GetBehavior();
	BotBehaviorState NewBehavior = BotsSharedState::GetInstance().GetBehaviorState();

	bool bBehaviorChanged = OldBehavior != NewBehavior;

	if (bBehaviorChanged)
	{
		Bot->SetBehavior(NewBehavior);
	}

	switch (NewBehavior)
	{
	case BotBehaviorState::HuntingForBait:
		MoveToActor(Bait);
		break;
	case BotBehaviorState::HuntingForPlayer:
		Cast<ABot>(GetPawn())->UpdateBaitLocation();
		MoveToActor(Player);
		break;
	case BotBehaviorState::Surrounding:
		if (bBehaviorChanged)
		{
			StartSurrounding();
		}
		else
		{
			ContinueSurrounding();
		}
		break;
	case BotBehaviorState::Waiting:
		if (bBehaviorChanged)
		{
			StartWaiting();
		}
		break;
	default:
		UE_LOG(LogTemp, Display, TEXT("Unimplemented bot behavior"));
		return;
	}
}

void ABotController::StartSurrounding()
{
	Player->SetBlocked(true);

	float X;
	float Y;
	float Angle = 2.0f * PI * Index / Count;
	FMath::SinCos(&X, &Y, Angle);
	SurroundTargetLocation = Player->GetActorLocation() +
							 FVector(PlayerSurroundRadius * X, PlayerSurroundRadius * Y, 0.0f);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ABotController::StopSurrounding, TimeoutAfterCatch);
}

void ABotController::ContinueSurrounding()
{
	MoveToLocation(SurroundTargetLocation);
}

void ABotController::StopSurrounding()
{
	if (BotsSharedState::GetInstance().GetBehaviorState() == BotBehaviorState::Surrounding)
	{
		Player->SetBlocked(false);
		BotsSharedState::GetInstance().SetNextBehaviorState();
	}
}

void ABotController::StartWaiting()
{
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ABotController::StopWaiting, WaitingDuration);
}

void ABotController::StopWaiting()
{
	if (BotsSharedState::GetInstance().GetBehaviorState() == BotBehaviorState::Waiting)
	{
		BotsSharedState::GetInstance().SetNextBehaviorState();
	}
}

template <typename T>
T *FindFirstActorOnScene(UWorld *World)
{
	TArray<AActor *> Actors;
	UGameplayStatics::GetAllActorsOfClass(World, T::StaticClass(), Actors);
	for (AActor *Actor : Actors)
	{
		T *MaybeActor = Cast<T>(Actor);
		if (MaybeActor != nullptr)
		{
			return MaybeActor;
		}
	}

	return nullptr;
}
