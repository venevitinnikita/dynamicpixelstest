#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Bait.generated.h"

UCLASS()
class DYNAMICPIXELS_API ABait : public AActor
{
	GENERATED_BODY()

  private:
	bool bOwned;

	UFUNCTION()
	void OnStartOverlap(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult);

	UFUNCTION()
	void OnEndOverlap(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex);

  public:
	ABait();

	void SetOwned(bool bOwned);
	bool IsOwned();
};
