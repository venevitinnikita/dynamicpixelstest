#include "Bot.h"

ABot::ABot()
{
	ThrowDistance = 150.0f;
}

void ABot::AllowGrabBait(ABait *Bait)
{
	if (BehaviorState == BotBehaviorState::HuntingForBait)
	{
		Hunter::AllowGrabBait(Bait);
		Hunter::Grab();
	}
}

void ABot::SetBehavior(BotBehaviorState BehaviorState)
{
	this->BehaviorState = BehaviorState;
}

BotBehaviorState ABot::GetBehavior()
{
	return BehaviorState;
}

void ABot::UpdateBaitLocation()
{
	Hunter::UpdateSelfPosition(GetActorLocation(), GetActorRotation());
	Hunter::UpdateBaitLocation();
}
