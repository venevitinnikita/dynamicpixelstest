#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TimerManager.h"
#include "Bot.h"
#include "PlayerCharacter.h"
#include "BotBehavior.h"
#include "BotsSharedState.h"
#include "BotController.generated.h"

UCLASS()
class DYNAMICPIXELS_API ABotController : public AAIController
{
	GENERATED_BODY()

private:
	static int Count;
	int Index;

	APlayerCharacter *Player;
	ABait *Bait;

	FVector SurroundTargetLocation;
	FTimerHandle TimerHandle;

	void StartSurrounding();
	void ContinueSurrounding();
	void StopSurrounding();

	void StartWaiting();
	void StopWaiting();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float WaitingDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float TimeoutAfterCatch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float PlayerCatchDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float PlayerSurroundRadius;

	ABotController();

	virtual void Tick(float DeltaTime) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult &Result) override;
};
